variable "my-access-key" {
  type = string
}

variable "my-secret-key" {
  type = string
}

variable "region-zone" {
  type = string
}

variable "aws_iam_role_name" {
  type = string
}

variable "aws_iam_role_arn" {
  type = string
}

variable "function_name" {
  type = string
}

variable "bucket-name" {
  type = string
}

variable "bucket-filename" {
  type = string
}


