resource "aws_lambda_function" "test_lambda" {
  s3_bucket = var.bucket-name
  s3_key = var.bucket-filename
  function_name = var.function_name
  role          = var.aws_iam_role_arn
  handler       = "exports.test"
  runtime = "nodejs12.x"
  environment {
    variables = {
      foo = "bar"
    }
  }
  timeout          = 180 
}
