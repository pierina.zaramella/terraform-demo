variable "bitbucket_user" {
  type = string
}

variable "bitbucket_pass" {
  type = string
}

variable "bitbucket_test_lambda_owner" {
  type = string
}

variable "bitbucket_test_lambda_name" {
  type = string
}

variable "bitbucket_test_lambda_scm" {
  type = string
}

variable "s3-bucket-arn" {
  type = string
}

