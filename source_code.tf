# Configure the Bitbucket Provider
provider "bitbucket" {
  username = var.bitbucket_user
  password = var.bitbucket_pass # you can also use app passwords
}

resource "bitbucket_repository" "illusions" {
  owner      = var.bitbucket_user
  name       = var.bitbucket_user
  scm        = "git"
  is_private = true
}