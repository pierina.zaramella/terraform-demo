provider "aws" {
  region     = var.region-zone
  access_key = var.my-access-key
  secret_key = var.my-secret-key  
}

/* resource "aws_vpc" "example" {
  cidr_block = "10.0.0.0/16"
 }
 AWS_CONTAINER_CREDENTIALS_RELATIVE_URI */


/* 
resource "aws_lambda_event_source_mapping" "example" {
  event_source_arn = "${aws_sqs_queue.sqs_queue_test.arn}"
  function_name    = "${aws_lambda_function.example.arn}"
} */
